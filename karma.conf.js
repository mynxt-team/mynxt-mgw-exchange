var gulpfile = require('./Gulpfile');

module.exports = function (config) {
  config.set({
    basePath: './',
    frameworks: ['jasmine'],
    browsers: ['PhantomJS'],
    files: gulpfile.paths.js.concat([
      './src/config.js',
      './bower_components/angular-mocks/angular-mocks.js',
      './test/unit/**/*.js'
    ])
  });
};