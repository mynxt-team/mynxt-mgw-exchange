'use strict';

angular.module('mynxt-mgw-exchange', [
  'ui.bootstrap',
  'angular-ladda',
  'angular-loading-bar',
  'cgNotify'
])
  .constant('appConfig', {
    apiURL: 'https://wallet.mynxt.info',
    sendFees: 0.2
  })
  .config(function (laddaProvider, cfpLoadingBarProvider) {
    laddaProvider.setOption({
      style: 'zoom-out'
    });

    cfpLoadingBarProvider.includeSpinner = false;
  });
