'use strict';

angular.module('mynxt-mgw-exchange')
  .service('MyNXT', [
    '$http', '$window', '$q', 'NRS', function ($http, $window, $q, NRS) {
      var MyNXT = this;
      MyNXT.accounts = [];
      MyNXT.selectedAccount = {};
      MyNXT.selectedAccountData = {};

      var environment = 'production';
      var walletURL = 'https://wallet.mynxt.info';
      var apiPath = '/api/0.1/';
      var blockExplorerURL = 'https://mynxt.info/api/0.1/public/index.php/';

      if ($window.location.origin.indexOf('mynxt') !== -1) {
        environment = 'production';
        walletURL = $window.location.origin;
      } else {
        environment = 'development';
      }

      MyNXT.apiURL = walletURL + apiPath;

      MyNXT.getAccounts = function () {
        var deferred = $q.defer();
        var url = walletURL + apiPath + 'user/account';
        var params = {};

        if (environment === 'development') {
          params.email = MyNXTConfig.email;
          params.password = MyNXTConfig.password;
        }

        $http({
          url: url,
          params: params
        }).success(function (result) {
          if (result && result.data && result.data.accounts) {
            MyNXT.accounts = result.data.accounts;
            MyNXT.selectedAccount = MyNXT.findSelectedAccount(MyNXT.accounts);

            deferred.resolve(MyNXT.accounts);
          }
        });

        return deferred.promise;
      };

      MyNXT.changeSelectedAccount = function (account) {
        if (!account.tx_account_id) throw 'missing tx_account_id parameter';

        var deferred = $q.defer();

        MyNXT.selectedAccount = account;
        MyNXT.getAccountData()
          .then(deferred.resolve);

        return deferred.promise;
      };

      MyNXT.findSelectedAccount = function (accounts) {
        for (var i = 0; i < accounts.length; i++) {
          var account = accounts[i];
          if (account.bl_selected == 1) {
            return account;
          }
        }
      };

      MyNXT.getAccountData = function (account) {
        if (!account) account = MyNXT.selectedAccount.tx_account_id;

        var deferred = $q.defer();

        NRS.makeRequest({
          requestType: 'getAccount',
          account: account
        }).success(function (result) {
          if (result && !result.errorCode) {
            MyNXT.selectedAccountData = result;
          }
          deferred.resolve(result);
        });

        return deferred.promise;
      };

      MyNXT.findAssetBalance = function (asset) {
        if (!asset) throw 'missing asset parameter';

        var balances = MyNXT.selectedAccountData.unconfirmedAssetBalances;
        if (balances) {
          for (var i = 0; i < balances.length; i++) {
            var balance = balances[i];

            if (balance.asset == asset) {
              return Number(balance.unconfirmedBalanceQNT);
            }
          }
        }

        return 0;
      };

      MyNXT.queryExplorer = function (requestType, params) {
        if (!requestType) throw 'requestType missing';

        if (!params) params = {};

        return $http({
          method: 'GET',
          url: blockExplorerURL + requestType,
          params: params
        });
      };

      MyNXT.sendTransaction = function (data, callback) {
        var messageId = Math.floor(Math.random() * 10000000);
        data.messageId = messageId;

        parent.postMessage(data, walletURL);

        function listener (event) {
          if (event.origin !== walletURL) return;

          if (event.data && event.data.messageId == messageId) {
            delete event.data.messageId;
            callback(event.data);
          }

          window.removeEventListener('message', listener, false);
        }

        window.addEventListener('message', listener, false);
      };
    }
  ]);