'use strict';

angular.module('mynxt-mgw-exchange')
  .service('NRS', ['$http', 'appConfig', function ($http, appConfig) {
    this.makeRequest = function (data) {
      if (typeof MyNXTConfig !== 'undefined') {
        data.email = MyNXTConfig.email;
        data.password = MyNXTConfig.password;
      }

      return $http({
        method: 'POST',
        url: appConfig.apiURL + '/nxt',
        params: data
      });
    };
  }]);