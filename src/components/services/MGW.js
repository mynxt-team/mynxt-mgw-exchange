'use strict';

angular.module('mynxt-mgw-exchange')
  .service('MGW', [
    'NRS', '$q', '$http', 'MyNXT', function (NRS, $q, $http, MyNXT) {
      var MGW = this;
      MGW.coins = [
        {
          name: 'mgwBTC',
          asset: '17554243582654188572',
          issuer: 'NXT-3TKA-UH62-478B-DQU6K',
          coin: 'BTC',
          coinName: 'Bitcoin',
          decimals: 8,
          min: 0.0005,
          fee: 0.0002,
          step: 0.00000001
        }
      ];
      MGW.selectedCoin = this.coins[0];
      MGW.askOrders = [];

      MGW.getAskOrders = function (asset) {
        var deferred = $q.defer();

        var params = {
          requestType: 'getAskOrders',
          asset: asset
        };

        NRS.makeRequest(params)
          .success(function (result) {
            if (result && result.askOrders) {
              MGW.askOrders = result.askOrders;
              deferred.resolve(true);
            }
          });

        return deferred.promise;
      };

      MGW.getStatus = function () {
        var url = MyNXT.apiURL + 'mgw/status';
        var params = {
          coin: this.selectedCoin.name
        };
        return $http({
          method: 'GET',
          url: url,
          params: params
        });
      };

      MGW.calculateOrderPrice = function (orders, neededQNT) {
        if (!orders) orders = MGW.askOrders;

        var totalPriceNQT = 0;
        var totalQuantityQNT = 0;
        var priceNQT = 0;
        neededQNT = Number(neededQNT);

        var ordersCopy = orders.slice(); // copy to prevent changing original

        while (neededQNT > 0 && ordersCopy[0]) {
          priceNQT = Number(ordersCopy[0].priceNQT);
          var quantityQNT = Number(ordersCopy[0].quantityQNT);

          if (ordersCopy[0].quantityQNT >= neededQNT) {
            totalPriceNQT += BigNumber(neededQNT).times(priceNQT).toNumber();
            totalQuantityQNT += neededQNT;
            neededQNT -= quantityQNT;
          } else {
            totalPriceNQT += BigNumber(quantityQNT).times(priceNQT).toNumber();
            totalQuantityQNT += quantityQNT;
            ordersCopy.splice(0, 1);
            neededQNT -= quantityQNT;
          }
        }

        return {
          priceNQT: priceNQT,
          totalPriceNQT: totalPriceNQT,
          totalQuantityQNT: totalQuantityQNT
        };
      };
    }
  ]);