'use strict';

angular.module('mynxt-mgw-exchange')
  .controller('SendController', function ($scope, $rootScope, MyNXT, MGW, appConfig, $interval, notify) {
    var controller = this;

    $scope.coin = MGW.selectedCoin;
    $scope.sendForm = {
      recipient: '',
      amount: 0,
      amountToBuy: 0,
      coinPrice: 0,
      totalCost: 0,
      fees: 0,
      loading: false
    };

    getAskOrders();

    $interval(function () {
      getAskOrders();
    }, 60000);

    $scope.submitForm = function () {
      $scope.sendForm.loading = true;

      getAskOrders()
        .then(function () {
          var data = {};
          var amount = Number($scope.sendForm.amount);
          amount = BigNumber(amount).plus(MGW.selectedCoin.fee).toNumber();

          var neededQNT = controller.calculateNeededQNT(amount);
          var quantityQNT = BigNumber(amount).times(Math.pow(10, MGW.selectedCoin.decimals)).toNumber();
          var withdrawalRecipient = $scope.sendForm.recipient.trim();

          var sendData = {
            requestType: 'transferAsset',
            quantityQNT: quantityQNT,
            asset: MGW.selectedCoin.asset,
            recipient: MGW.selectedCoin.issuer,
            message: '{"redeem":"' + MGW.selectedCoin.coin + '","withdrawaddr":"' + withdrawalRecipient + '","MyNXT":"Withdrawal via the MyNXT MGW exchange plugin"}',
            account: MyNXT.selectedAccount.tx_account_id
          };

          var orderPrice = controller.calculateOrderPrice(amount);
          var myNxtFee = controller.calculateFee(orderPrice) - 3;

          if (neededQNT > 0) {
            // we need to buy BTC
            var priceNQT = MGW.calculateOrderPrice(MGW.askOrders, neededQNT).priceNQT;

            data = {
              MGW: sendData,
              requestType: 'placeBidOrder',
              asset: MGW.selectedCoin.asset,
              quantityQNT: neededQNT,
              priceNQT: priceNQT,
              account: MyNXT.selectedAccount.tx_account_id,
              myNxtFee: myNxtFee
            };
          } else {
            // we have enough BTC
            sendData.myNxtFee = myNxtFee;
            data = sendData;
          }

          MyNXT.sendTransaction(data, function (result) {
            $scope.sendForm.loading = false;

            if (result && result.transaction) {
              var message = 'Successfully sent ' + $scope.coin.coin + '. Updating your balance can take a couple minutes.';
              notify(message);

              getAskOrders();
              MyNXT.getAccountData();
              $rootScope.refresh();

              $scope.sendForm.recipient = '';
              $scope.sendForm.amount = 0;
              $scope.sendForm.amountToBuy = 0;
              $scope.sendForm.totalCost = 0;
            }

            $scope.$apply();
          });
        });
    };

    $scope.hasEnoughBalance = function () {
      var balanceNXT = BigNumber(Number($scope.selectedAccountData.unconfirmedBalanceNQT)).dividedBy(Math.pow(10, 8)).toNumber();
      return balanceNXT > Number($scope.sendForm.totalCost);
    };

    $scope.sendAmountChanged = function (amount) {
      if (!amount) return;

      amount = Number(amount);
      amount = BigNumber(amount).plus(MGW.selectedCoin.fee).toNumber();

      var orderPrice = controller.calculateOrderPrice(amount);
      var fee = controller.calculateFee(orderPrice);

      $scope.sendForm.fees = fee;
      $scope.sendForm.amountToBuy = controller.calculateNeededQNT(amount);
      $scope.sendForm.totalCost = (orderPrice + fee).toFixed(2);
    };

    controller.calculateNeededQNT = function (amount) {
      var balanceQNT = MyNXT.findAssetBalance($scope.coin.asset); // what we have now
      var neededQNT = BigNumber(amount).times(Math.pow(10, $scope.coin.decimals)).minus(balanceQNT).toNumber(); // what we need

      if (neededQNT < 0) {
        return 0;
      }

      return neededQNT;
    };

    controller.calculateOrderPrice = function (amount) {
      var neededQNT = controller.calculateNeededQNT(amount);
      var orderPriceNQT = MGW.calculateOrderPrice(MGW.askOrders, neededQNT).totalPriceNQT;
      var orderPrice = BigNumber(orderPriceNQT).dividedBy(Math.pow(10, $scope.coin.decimals)).toNumber();

      return orderPrice;
    };

    controller.calculateFee = function (orderPrice) {
      var fee = BigNumber(orderPrice).times(appConfig.sendFees / 100).toNumber();

      if (fee < 1) {
        fee = 1;
      }

      fee += 3; // 1 NXT for assetTransfer, 1 NXT for sendMoney // 1 NXT for placeBidOrder

      return Number(fee.toFixed(2));
    };

    function getAskOrders () {
      return MGW.getAskOrders($scope.coin.asset)
        .then(function () {
          var coinPriceNQT = MGW.calculateOrderPrice(MGW.askOrders, Math.pow(10, $scope.coin.decimals)).totalPriceNQT;
          var coinPrice = BigNumber(coinPriceNQT).dividedBy(Math.pow(10, $scope.coin.decimals)).toNumber();
          $scope.sendForm.coinPrice = Number(coinPrice.toFixed(2));
        });
    }
  });