'use strict';

angular.module('mynxt-mgw-exchange')
  .controller('AccountOrdersController', function ($scope, MyNXT, notify) {

    $scope.cancelOrder = function (order, type) {
      order.loading = true;

      var requestType = type === 'bid' ? 'cancelBidOrder' : 'cancelAskOrder';

      var params = {
        requestType: requestType,
        account: MyNXT.selectedAccount.tx_account_id,
        order: order.order
      };

      MyNXT.sendTransaction(params, function (result) {
        order.loading = false;
        if (result && result.transaction) {
          notify('Successfully cancelled order');
        }

        $scope.$apply();
      })
    };
  });