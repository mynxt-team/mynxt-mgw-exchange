'use strict';

angular.module('mynxt-mgw-exchange')
  .controller('OrderController', function ($scope, $modal, MGW, NRS, MyNXT, notify) {
    $scope.form = {
      quantity: 0,
      price: 0,
      total: 0,
      loading: false
    };
    $scope.orderType = 'buy';
    $scope.title = 'Buy';
    $scope.buttonClass = 'btn-success';

    $scope.initForm = function (orderType) {
      if (orderType === 'sell') {
        $scope.orderType = orderType;
        $scope.title = 'Sell';
        $scope.buttonClass = 'btn-danger';
      }
      getOrders();
    };

    $scope.submitForm = function () {
      $scope.form.loading = true;

      var requestType = $scope.orderType === 'buy' ? 'placeBidOrder' : 'placeAskOrder';
      var quantityQNT = BigNumber($scope.form.quantity).times(Math.pow(10, MGW.selectedCoin.decimals)).toNumber();
      var priceNQT = BigNumber($scope.form.price).times(Math.pow(10, 8)).dividedBy(Math.pow(10, MGW.selectedCoin.decimals)).toNumber();

      var params = {
        requestType: requestType,
        asset: MGW.selectedCoin.asset,
        quantityQNT: quantityQNT,
        priceNQT: priceNQT,
        account: MyNXT.selectedAccount.tx_account_id
      };

      MyNXT.sendTransaction(params, function (result) {
        $scope.form.loading = false;

        if (result && result.transaction) {
          var message = 'Successfully placed ' + $scope.orderType + ' order';
          notify(message);
        }

        $scope.$apply();
      });
    };

    $scope.fillOrder = function (order) {
      var quantity = BigNumber(order.quantityQNT).dividedBy(Math.pow(10, $scope.coin.decimals)).toNumber();
      var price = BigNumber(order.priceNQT).times(Math.pow(10, $scope.coin.decimals)).dividedBy(Math.pow(10, 8)).toNumber();

      $scope.form.quantity = quantity;
      $scope.form.price = price;
      $scope.onAmountChange();
    };

    $scope.onAmountChange = function () {
      var price = Number($scope.form.price);
      var quantity = Number($scope.form.quantity);
      var total = BigNumber(price).times(quantity).toNumber();
      if (isNaN(total)) total = 0;
      $scope.form.total = total;
    };

    function getOrders () {
      var requestType = $scope.orderType === 'buy' ? 'getAskOrders' : 'getBidOrders';
      var orders = $scope.orderType === 'buy' ? 'askOrders' : 'bidOrders';

      var params = {
        requestType: requestType,
        asset: MGW.selectedCoin.asset,
        firstIndex: 0,
        lastIndex: 2
      };

      NRS.makeRequest(params)
        .then(function (result) {
          if (result && result.data[orders] && result.data[orders].length) {
            $scope.orders = result.data[orders];
          }
        });
    }
  });