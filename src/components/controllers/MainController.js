'use strict';

angular.module('mynxt-mgw-exchange')
  .controller('MainController', function ($scope, $rootScope, $interval, MyNXT, MGW, NRS) {
    $scope.selectedAccount = {};
    $scope.accounts = [];
    $scope.selectedAccountData = {};
    $scope.askOrders = [];
    $scope.bidOrders = [];
    $scope.coinBalanceQNT = 0;
    $scope.coin = MGW.selectedCoin;
    $scope.collapseDetails = true;
    $scope.mgwStatus = 'online';
    $scope.mgwStatusClass = 'success';

    if ($scope.mgwStatus === 'offline') {
      $scope.mgwStatusClass = 'danger';
    }

    getAccountData();
    getMgwStatus();

    $interval(function () {
      MyNXT.getAccountData()
        .then(updateAccountData)
        .then(function () {
          getAccountOrders('buy');
          getAccountOrders('sell');
        });
    }, 60000);

    $scope.changeSelectedAccount = function (account) {
      $scope.selectedAccount = account;

      MyNXT.changeSelectedAccount(account)
        .then(updateAccountData)
        .then(function () {
          getAccountOrders('buy');
          getAccountOrders('sell');
        });
    };

    $rootScope.refresh = function () {
      MyNXT.getAccountData()
        .then(updateAccountData)
        .then(function () {
          getAccountOrders('buy');
          getAccountOrders('sell');
        });
    };

    function getAccountData () {
      return MyNXT.getAccounts()
        .then(function (accounts) {
          $scope.accounts = accounts;
          $scope.selectedAccount = MyNXT.selectedAccount;
        })
        .then(MyNXT.getAccountData)
        .then(updateAccountData)
        .then(function () {
          getAccountOrders('buy');
          getAccountOrders('sell');
        });
    }

    function getAccountOrders (type) {
      var requestType = type === 'buy' ? 'getAccountCurrentBidOrders' : 'getAccountCurrentAskOrders';
      var orders = type === 'buy' ? 'bidOrders' : 'askOrders';
      $scope[orders] = [];

      var params = {
        requestType: requestType,
        asset: MGW.selectedCoin.asset,
        account: MyNXT.selectedAccount.tx_account_id
      };

      NRS.makeRequest(params)
        .then(function (result) {
          if (result && result.data[orders] && result.data[orders].length) {
            $scope[orders] = result.data[orders];
          }
        });
    }

    function updateAccountData (result) {
      if (result && !result.errorCode) {
        $scope.selectedAccountData = result;
        $scope.coinBalanceQNT = MyNXT.findAssetBalance(MGW.selectedCoin.asset);
      } else {
        $scope.selectedAccountData = {unconfirmedBalanceNQT: 0};
        $scope.coinBalanceQNT = 0;
      }
    }

    function getMgwStatus () {
      MGW.getStatus()
        .then(function (result) {
          if (result && result.data.data) {
            if (result.data.data.bl_status == 1) {
              $scope.mgwStatus = 'online';
              $scope.mgwStatusClass = 'success';
            } else {
              $scope.mgwStatus = 'offline';
              $scope.mgwStatusClass = 'danger';
            }
          }
        });
    }
  });