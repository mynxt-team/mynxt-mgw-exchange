describe('start', function () {
  beforeEach(function () {
    browser.get('/');
  });

  it('should have a title', function () {
    expect(browser.getTitle()).toEqual('MyNXT MGW');
  });

  it('should fetch MGW balance', function (done) {
    browser.executeAsyncScript(function (callback) {
      var NRS = angular.injector(['ng', 'mynxt-mgw']).get('NRS');
      NRS.makeRequest({requestType: 'getConstants'})
        .success(function (result) {
          callback(result);
        });
    }).then(function (result) {
      expect(typeof result.errorCode).toBe('undefined');
      done();
    });
  })
});