describe('MyNXT service', function () {
  beforeEach(angular.mock.module('mynxt-mgw-exchange'));

  var MyNXT;
  var appConfig;
  var $httpBackend;

  beforeEach(angular.mock.inject(function (_MyNXT_, _appConfig_, _$httpBackend_) {
    MyNXT = _MyNXT_;
    appConfig = _appConfig_;
    $httpBackend = _$httpBackend_;

    $httpBackend.whenGET(/https:\/\/wallet.mynxt.info\/api\/0.1\/user\/account/).respond(Fixtures.getAccounts);
    $httpBackend.whenPOST(appConfig.apiURL + '/nxt?account=14305241101586882346&requestType=getAccountAssets').respond(Fixtures.getAccountAssets);
    $httpBackend.whenPOST(appConfig.apiURL + '/nxt?account=14305241101586882346&requestType=getAccount').respond(Fixtures.getAccount);
    $httpBackend.whenPOST(appConfig.apiURL + '/nxt?account=123&requestType=getAccount').respond(Fixtures.getAccount);
  }));

  it('should exist', function () {
    expect(MyNXT).not.toBe(null);
  });

  it('should set the correct selectedAccount', function () {
    var account = MyNXT.findSelectedAccount(Fixtures.getAccounts.data.accounts);
    expect(account.tx_account_id).toEqual('14305241101586882346');
  });

  it('should fetch user accounts', function () {
    MyNXT.getAccounts();
    $httpBackend.flush();

    expect(MyNXT.accounts.length).toEqual(2);
    expect(MyNXT.selectedAccount.tx_account_id).toEqual('14305241101586882346');
  });

  it('should retrieve account data', function () {
    MyNXT.getAccounts();
    $httpBackend.flush();

    MyNXT.getAccountData();
    $httpBackend.flush();

    expect(MyNXT.selectedAccountData.publicKey).toBe('0e89a92f502be03444defc640ebf4d44bbf911c42bff618d053493b51b347161');
    expect(MyNXT.selectedAccountData.assetBalances.length).toBe(4);
  });

  describe('changing account', function () {
    it('should change account and retrieve data', function () {
      var account = {
        tx_account_id: '123'
      };

      expect(MyNXT.selectedAccount).toEqual({});
      expect(MyNXT.selectedAccountData).toEqual({});

      MyNXT.changeSelectedAccount(account);
      $httpBackend.flush();

      expect(MyNXT.selectedAccount.tx_account_id).toBe('123');
      expect(MyNXT.selectedAccountData.assetBalances.length).toBe(4);
    });

    it('should only change to correct account', function () {
      expect(MyNXT.changeSelectedAccount).toThrow();
    });
  });

  describe('asset balances', function () {
    beforeEach(function () {
      MyNXT.getAccounts();
      $httpBackend.flush();

      MyNXT.getAccountData();
      $httpBackend.flush();
    });

    it('should find the correct asset balance', function () {
      var balance = Number(MyNXT.findAssetBalance('12071612744977229797'));
      expect(balance).toBe(300);
    });

    it('should return 0 if no asset can be found', function () {
      var balance = Number(MyNXT.findAssetBalance('123'));
      expect(balance).toBe(0);
    });

    it('should throw an error if no asset parameter is supplied', function () {
      expect(MyNXT.findAssetBalance).toThrow();
    })
  });
});