describe('NRS service', function () {
  var NRS;
  var appConfig;

  beforeEach(angular.mock.module('mynxt-mgw-exchange'));

  beforeEach(angular.mock.inject(function (_NRS_, _appConfig_) {
    NRS = _NRS_;
    appConfig = _appConfig_;
  }));

  it('should exist', function () {
    expect(NRS).not.toBe(null);
  });

  it('should have a request method', function () {
    expect(typeof NRS.makeRequest).toBe('function');
  });
});