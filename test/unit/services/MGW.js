describe('MyNXT service', function () {
  beforeEach(angular.mock.module('mynxt-mgw-exchange'));

  var MGW;

  beforeEach(angular.mock.inject(function (_MGW_) {
    MGW = _MGW_;
  }));

  it('should exist', function () {
    expect(MGW).not.toBe(null);
  });

  it('should have an array of coins', function () {
    expect(MGW.coins.length).not.toBe(0);
  });

  it('should correctly calculate amounts if the first order has enough', function () {
    var orders = [
      {
        "quantityQNT": "25000000",
        "priceNQT": "20000"
      }
    ];

    var amounts = MGW.calculateOrderPrice(orders, 25000000);
    expect(amounts.totalPriceNQT).toBe(500000000000);
    expect(amounts.totalQuantityQNT).toBe(25000000);

    amounts = MGW.calculateOrderPrice(orders, 20000000);
    expect(amounts.totalPriceNQT).toBe(400000000000);
    expect(amounts.totalQuantityQNT).toBe(20000000);
  });

  it('should correctly calculate price if more than one order is needed', function () {
    var orders = [
      {
        "quantityQNT": "20000000",
        "priceNQT": "20000"
      },
      {
        "quantityQNT": "5000000",
        "priceNQT": "25000"
      },
      {
        "quantityQNT": "10000000",
        "priceNQT": "30000"
      }
    ];

    var amounts = MGW.calculateOrderPrice(orders, 25000000);
    expect(amounts.totalPriceNQT).toBe(525000000000);
    expect(amounts.totalQuantityQNT).toBe(25000000);
    expect(amounts.priceNQT).toBe(25000);

    amounts = MGW.calculateOrderPrice(orders, 30000000);
    expect(amounts.totalPriceNQT).toBe(675000000000);
    expect(amounts.totalQuantityQNT).toBe(30000000);
    expect(amounts.priceNQT).toBe(30000);

    amounts = MGW.calculateOrderPrice(orders, 35000000);
    expect(amounts.totalPriceNQT).toBe(825000000000);
    expect(amounts.totalQuantityQNT).toBe(35000000);
    expect(amounts.priceNQT).toBe(30000);
  });

  it('should correctly calculate if not enough is available', function () {
    var orders = [
      {
        "quantityQNT": "20000000",
        "priceNQT": "20000"
      },
      {
        "quantityQNT": "5000000",
        "priceNQT": "25000"
      },
      {
        "quantityQNT": "10000000",
        "priceNQT": "30000"
      }
    ];

    var amounts = MGW.calculateOrderPrice(orders, 40000000);
    expect(amounts.totalPriceNQT).toBe(825000000000);
    expect(amounts.totalQuantityQNT).toBe(35000000);
  });
});