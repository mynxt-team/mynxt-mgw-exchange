describe('FormatNumbers', function () {
  beforeEach(angular.mock.module('mynxt-mgw-exchange'));

  it('should correctly calculate balance', function () {
    inject(function (formatQNTFilter) {
      expect(formatQNTFilter(Math.pow(10, 8), 8)).toBe(1);
      expect(formatQNTFilter(Math.pow(10, 8), 7)).toBe(10);
      expect(formatQNTFilter(Math.pow(10, 8), 6)).toBe(100);
      expect(formatQNTFilter(Math.pow(10, 8), 5)).toBe(1000);
      expect(formatQNTFilter(Math.pow(10, 8), 4)).toBe(10000);
      expect(formatQNTFilter(Math.pow(10, 8), 3)).toBe(100000);
      expect(formatQNTFilter(Math.pow(10, 8), 2)).toBe(1000000);
      expect(formatQNTFilter(Math.pow(10, 8), 1)).toBe(10000000);
      expect(formatQNTFilter(Math.pow(10, 8), 0)).toBe(100000000);
      expect(formatQNTFilter(Math.pow(10, 8))).toBe(100000000);
    });
  });

  it('should correctly calculate price', function () {
    inject(function (formatPriceNQTFilter) {
      expect(formatPriceNQTFilter(Math.pow(10, 8))).toBe(1);
      expect(formatPriceNQTFilter(Math.pow(10, 8), 8)).toBe(100000000);
      expect(formatPriceNQTFilter(Math.pow(10, 8), 7)).toBe(10000000);
      expect(formatPriceNQTFilter(Math.pow(10, 8), 6)).toBe(1000000);
      expect(formatPriceNQTFilter(Math.pow(10, 8), 5)).toBe(100000);
      expect(formatPriceNQTFilter(Math.pow(10, 8), 4)).toBe(10000);
      expect(formatPriceNQTFilter(Math.pow(10, 8), 3)).toBe(1000);
      expect(formatPriceNQTFilter(Math.pow(10, 8), 2)).toBe(100);
      expect(formatPriceNQTFilter(Math.pow(10, 8), 1)).toBe(10);
      expect(formatPriceNQTFilter(Math.pow(10, 8), 0)).toBe(1);
    });
  });
});