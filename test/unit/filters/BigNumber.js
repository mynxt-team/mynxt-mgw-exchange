describe('BigNumber', function () {
  beforeEach(angular.mock.module('mynxt-mgw-exchange'));

  it('should correctly multiply', function () {
    inject(function (timesFilter) {
      expect(0.1 * 0.1).not.toBe(0.01);
      expect(timesFilter(0.1, 0.1)).toBe(0.01);
    });
  });

  it('should correctly divide', function () {
    inject(function (divideFilter) {
      expect(0.3 / 0.2).not.toBe(1.5);
      expect(divideFilter(0.3, 0.2)).toBe(1.5);
    });
  });
});