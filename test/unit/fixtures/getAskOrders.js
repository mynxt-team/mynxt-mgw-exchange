var Fixtures = Fixtures || {};

Fixtures.getAskOrders = {
  "requestProcessingTime": 21,
  "askOrders": [
    {
      "height": 367507,
      "priceNQT": "18940",
      "asset": "17554243582654188572",
      "order": "5134485862266526478",
      "quantityQNT": "88440000",
      "accountRS": "NXT-2UKS-7VYN-Q73Y-EKE8Y",
      "account": "14923118471272229432",
      "type": "ask"
    },
    {
      "height": 367779,
      "priceNQT": "19000",
      "asset": "17554243582654188572",
      "order": "2048238259108724129",
      "quantityQNT": "50000000",
      "accountRS": "NXT-YBRH-UZV8-XV99-BPPP6",
      "account": "10544815516691670767",
      "type": "ask"
    },
    {
      "height": 367779,
      "priceNQT": "19200",
      "asset": "17554243582654188572",
      "order": "8791974133597007188",
      "quantityQNT": "50000000",
      "accountRS": "NXT-YBRH-UZV8-XV99-BPPP6",
      "account": "10544815516691670767",
      "type": "ask"
    },
    {
      "height": 367779,
      "priceNQT": "19400",
      "asset": "17554243582654188572",
      "order": "18204972162818290781",
      "quantityQNT": "50000000",
      "accountRS": "NXT-YBRH-UZV8-XV99-BPPP6",
      "account": "10544815516691670767",
      "type": "ask"
    },
    {
      "height": 368114,
      "priceNQT": "19441",
      "asset": "17554243582654188572",
      "order": "10236798617757195288",
      "quantityQNT": "20000000",
      "accountRS": "NXT-QMAH-K244-TM35-A7ZXK",
      "account": "9869609480753990927",
      "type": "ask"
    },
    {
      "height": 367404,
      "priceNQT": "19500",
      "asset": "17554243582654188572",
      "order": "16679453252551911373",
      "quantityQNT": "500000000",
      "accountRS": "NXT-5Z35-ZCDV-BC28-7KL5F",
      "account": "6237012670683413539",
      "type": "ask"
    },
    {
      "height": 366992,
      "priceNQT": "19595",
      "asset": "17554243582654188572",
      "order": "18357372511612975344",
      "quantityQNT": "68098611",
      "accountRS": "NXT-J2MS-AZL3-85VQ-DBRDQ",
      "account": "13487974428999942776",
      "type": "ask"
    },
    {
      "height": 367779,
      "priceNQT": "19600",
      "asset": "17554243582654188572",
      "order": "9549097291182239637",
      "quantityQNT": "50000000",
      "accountRS": "NXT-YBRH-UZV8-XV99-BPPP6",
      "account": "10544815516691670767",
      "type": "ask"
    },
    {
      "height": 367779,
      "priceNQT": "19800",
      "asset": "17554243582654188572",
      "order": "3925566364690022238",
      "quantityQNT": "50000000",
      "accountRS": "NXT-YBRH-UZV8-XV99-BPPP6",
      "account": "10544815516691670767",
      "type": "ask"
    },
    {
      "height": 367872,
      "priceNQT": "19999",
      "asset": "17554243582654188572",
      "order": "11143597339279809236",
      "quantityQNT": "239000000",
      "accountRS": "NXT-VZYH-HHXP-RRMJ-B8ADB",
      "account": "10713226218152263631",
      "type": "ask"
    },
    {
      "height": 366638,
      "priceNQT": "20000",
      "asset": "17554243582654188572",
      "order": "15694161414064594054",
      "quantityQNT": "976945590",
      "accountRS": "NXT-GX4W-RXGW-7PG5-3AJ84",
      "account": "1232307066044380252",
      "type": "ask"
    },
    {
      "height": 367779,
      "priceNQT": "20000",
      "asset": "17554243582654188572",
      "order": "17616031640222469073",
      "quantityQNT": "50000000",
      "accountRS": "NXT-YBRH-UZV8-XV99-BPPP6",
      "account": "10544815516691670767",
      "type": "ask"
    },
    {
      "height": 367779,
      "priceNQT": "20200",
      "asset": "17554243582654188572",
      "order": "10218495770975812461",
      "quantityQNT": "50000000",
      "accountRS": "NXT-YBRH-UZV8-XV99-BPPP6",
      "account": "10544815516691670767",
      "type": "ask"
    },
    {
      "height": 367779,
      "priceNQT": "20400",
      "asset": "17554243582654188572",
      "order": "5260511377620678506",
      "quantityQNT": "50000000",
      "accountRS": "NXT-YBRH-UZV8-XV99-BPPP6",
      "account": "10544815516691670767",
      "type": "ask"
    },
    {
      "height": 367779,
      "priceNQT": "20600",
      "asset": "17554243582654188572",
      "order": "17802828368172895412",
      "quantityQNT": "50000000",
      "accountRS": "NXT-YBRH-UZV8-XV99-BPPP6",
      "account": "10544815516691670767",
      "type": "ask"
    },
    {
      "height": 367779,
      "priceNQT": "20800",
      "asset": "17554243582654188572",
      "order": "12525507313016037312",
      "quantityQNT": "48860598",
      "accountRS": "NXT-YBRH-UZV8-XV99-BPPP6",
      "account": "10544815516691670767",
      "type": "ask"
    },
    {
      "height": 366599,
      "priceNQT": "20875",
      "asset": "17554243582654188572",
      "order": "2941034990823873949",
      "quantityQNT": "20000000",
      "accountRS": "NXT-QMAH-K244-TM35-A7ZXK",
      "account": "9869609480753990927",
      "type": "ask"
    },
    {
      "height": 366336,
      "priceNQT": "20880",
      "asset": "17554243582654188572",
      "order": "17801295312416193806",
      "quantityQNT": "51000000",
      "accountRS": "NXT-X44L-6YEG-ZV5S-FBM7T",
      "account": "15895007553641154642",
      "type": "ask"
    },
    {
      "height": 366105,
      "priceNQT": "20899",
      "asset": "17554243582654188572",
      "order": "11625345199031624181",
      "quantityQNT": "779570011",
      "accountRS": "NXT-GX4W-RXGW-7PG5-3AJ84",
      "account": "1232307066044380252",
      "type": "ask"
    },
    {
      "height": 366027,
      "priceNQT": "20900",
      "asset": "17554243582654188572",
      "order": "10662699176908254639",
      "quantityQNT": "12080000",
      "accountRS": "NXT-X44L-6YEG-ZV5S-FBM7T",
      "account": "15895007553641154642",
      "type": "ask"
    },
    {
      "height": 366101,
      "priceNQT": "20900",
      "asset": "17554243582654188572",
      "order": "5727973953223203010",
      "quantityQNT": "112080000",
      "accountRS": "NXT-X44L-6YEG-ZV5S-FBM7T",
      "account": "15895007553641154642",
      "type": "ask"
    },
    {
      "height": 365415,
      "priceNQT": "20972",
      "asset": "17554243582654188572",
      "order": "601789486186016352",
      "quantityQNT": "29500000",
      "accountRS": "NXT-2UKS-7VYN-Q73Y-EKE8Y",
      "account": "14923118471272229432",
      "type": "ask"
    },
    {
      "height": 365386,
      "priceNQT": "21100",
      "asset": "17554243582654188572",
      "order": "7736406254841424871",
      "quantityQNT": "1100000",
      "accountRS": "NXT-KXBT-62PV-ZEH8-ABFXF",
      "account": "9724864926014043449",
      "type": "ask"
    },
    {
      "height": 365488,
      "priceNQT": "21199",
      "asset": "17554243582654188572",
      "order": "12273863633250155001",
      "quantityQNT": "100000000",
      "accountRS": "NXT-ADH7-TCCK-R46P-AHZSR",
      "account": "10080164044348861925",
      "type": "ask"
    },
    {
      "height": 365081,
      "priceNQT": "21700",
      "asset": "17554243582654188572",
      "order": "16336687767063102634",
      "quantityQNT": "497553117",
      "accountRS": "NXT-GX4W-RXGW-7PG5-3AJ84",
      "account": "1232307066044380252",
      "type": "ask"
    },
    {
      "height": 365895,
      "priceNQT": "21789",
      "asset": "17554243582654188572",
      "order": "6622511626498228776",
      "quantityQNT": "64404829",
      "accountRS": "NXT-J2MS-AZL3-85VQ-DBRDQ",
      "account": "13487974428999942776",
      "type": "ask"
    },
    {
      "height": 367073,
      "priceNQT": "21791",
      "asset": "17554243582654188572",
      "order": "17427642862703084274",
      "quantityQNT": "20000000",
      "accountRS": "NXT-QMAH-K244-TM35-A7ZXK",
      "account": "9869609480753990927",
      "type": "ask"
    },
    {
      "height": 363612,
      "priceNQT": "22455",
      "asset": "17554243582654188572",
      "order": "10775753315432232251",
      "quantityQNT": "7278806",
      "accountRS": "NXT-QMAH-K244-TM35-A7ZXK",
      "account": "9869609480753990927",
      "type": "ask"
    },
    {
      "height": 365522,
      "priceNQT": "22498",
      "asset": "17554243582654188572",
      "order": "10653410119679465715",
      "quantityQNT": "100000000",
      "accountRS": "NXT-ADH7-TCCK-R46P-AHZSR",
      "account": "10080164044348861925",
      "type": "ask"
    },
    {
      "height": 362667,
      "priceNQT": "22499",
      "asset": "17554243582654188572",
      "order": "13391002799691761887",
      "quantityQNT": "2000000000",
      "accountRS": "NXT-GX4W-RXGW-7PG5-3AJ84",
      "account": "1232307066044380252",
      "type": "ask"
    },
    {
      "height": 361024,
      "priceNQT": "24885",
      "asset": "17554243582654188572",
      "order": "4367130329036666695",
      "quantityQNT": "20000000",
      "accountRS": "NXT-QMAH-K244-TM35-A7ZXK",
      "account": "9869609480753990927",
      "type": "ask"
    },
    {
      "height": 360989,
      "priceNQT": "25000",
      "asset": "17554243582654188572",
      "order": "3818849801167136161",
      "quantityQNT": "100000000",
      "accountRS": "NXT-GK8K-SV48-TRPV-8Z833",
      "account": "6954929769358509265",
      "type": "ask"
    },
    {
      "height": 364634,
      "priceNQT": "30000",
      "asset": "17554243582654188572",
      "order": "18282513722839285904",
      "quantityQNT": "25000000",
      "accountRS": "NXT-FG7F-2W46-M7DE-EHPV5",
      "account": "13974299138448603309",
      "type": "ask"
    },
    {
      "height": 334282,
      "priceNQT": "40000",
      "asset": "17554243582654188572",
      "order": "3545444239044461477",
      "quantityQNT": "15000",
      "accountRS": "NXT-W36X-E5NS-9424-4M9U7",
      "account": "2515527989447230621",
      "type": "ask"
    },
    {
      "height": 284412,
      "priceNQT": "42800",
      "asset": "17554243582654188572",
      "order": "4936351576393009412",
      "quantityQNT": "10000000",
      "accountRS": "NXT-A6DL-4GN2-SPA2-A3KKL",
      "account": "9891629999872807282",
      "type": "ask"
    },
    {
      "height": 284412,
      "priceNQT": "120000",
      "asset": "17554243582654188572",
      "order": "2074074296537052456",
      "quantityQNT": "10000000",
      "accountRS": "NXT-A6DL-4GN2-SPA2-A3KKL",
      "account": "9891629999872807282",
      "type": "ask"
    }
  ]
}