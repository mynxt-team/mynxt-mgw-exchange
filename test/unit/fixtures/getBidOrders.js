var Fixtures = Fixtures || {};

Fixtures.getBidOrders = {
  "requestProcessingTime": 28,
  "bidOrders": [
    {
      "height": 367779,
      "priceNQT": "18200",
      "asset": "17554243582654188572",
      "order": "13311334510093807406",
      "quantityQNT": "6963415",
      "accountRS": "NXT-YBRH-UZV8-XV99-BPPP6",
      "account": "10544815516691670767",
      "type": "bid"
    },
    {
      "height": 367732,
      "priceNQT": "18111",
      "asset": "17554243582654188572",
      "order": "11241818269823995435",
      "quantityQNT": "6000000",
      "accountRS": "NXT-QSTF-NN4F-LJAU-H7XKP",
      "account": "18070594158017078061",
      "type": "bid"
    },
    {
      "height": 367743,
      "priceNQT": "18007",
      "asset": "17554243582654188572",
      "order": "3575515448141407609",
      "quantityQNT": "200000000",
      "accountRS": "NXT-5A9B-Q2UK-6DGQ-BYDWW",
      "account": "11417045825630609641",
      "type": "bid"
    },
    {
      "height": 367695,
      "priceNQT": "18006",
      "asset": "17554243582654188572",
      "order": "4552264965929352408",
      "quantityQNT": "100000000",
      "accountRS": "NXT-X5EB-VSL5-Z7DU-3GWTJ",
      "account": "1758531264253431177",
      "type": "bid"
    },
    {
      "height": 367695,
      "priceNQT": "18005",
      "asset": "17554243582654188572",
      "order": "13557671713931193191",
      "quantityQNT": "100000000",
      "accountRS": "NXT-X5EB-VSL5-Z7DU-3GWTJ",
      "account": "1758531264253431177",
      "type": "bid"
    },
    {
      "height": 367596,
      "priceNQT": "18003",
      "asset": "17554243582654188572",
      "order": "4893059771532614495",
      "quantityQNT": "100000000",
      "accountRS": "NXT-X5EB-VSL5-Z7DU-3GWTJ",
      "account": "1758531264253431177",
      "type": "bid"
    },
    {
      "height": 367548,
      "priceNQT": "18002",
      "asset": "17554243582654188572",
      "order": "12827895074951825625",
      "quantityQNT": "100000000",
      "accountRS": "NXT-MU8M-98C9-GY89-3VJRH",
      "account": "1719842041820211411",
      "type": "bid"
    },
    {
      "height": 367779,
      "priceNQT": "18000",
      "asset": "17554243582654188572",
      "order": "245093396147482129",
      "quantityQNT": "50000000",
      "accountRS": "NXT-YBRH-UZV8-XV99-BPPP6",
      "account": "10544815516691670767",
      "type": "bid"
    },
    {
      "height": 347230,
      "priceNQT": "17988",
      "asset": "17554243582654188572",
      "order": "4918110820757526159",
      "quantityQNT": "100000000",
      "accountRS": "NXT-KTTD-E3CK-W4A6-FH3TP",
      "account": "15772783885749380907",
      "type": "bid"
    },
    {
      "height": 343865,
      "priceNQT": "17958",
      "asset": "17554243582654188572",
      "order": "9046785985464391374",
      "quantityQNT": "100000000",
      "accountRS": "NXT-WERD-X5BL-Y7U5-FPAET",
      "account": "15902515848941548267",
      "type": "bid"
    },
    {
      "height": 342701,
      "priceNQT": "17800",
      "asset": "17554243582654188572",
      "order": "13343395237290662455",
      "quantityQNT": "50000000",
      "accountRS": "NXT-KTTD-E3CK-W4A6-FH3TP",
      "account": "15772783885749380907",
      "type": "bid"
    },
    {
      "height": 367779,
      "priceNQT": "17800",
      "asset": "17554243582654188572",
      "order": "14492951418949516157",
      "quantityQNT": "50000000",
      "accountRS": "NXT-YBRH-UZV8-XV99-BPPP6",
      "account": "10544815516691670767",
      "type": "bid"
    },
    {
      "height": 367779,
      "priceNQT": "17600",
      "asset": "17554243582654188572",
      "order": "9204930260514635630",
      "quantityQNT": "50000000",
      "accountRS": "NXT-YBRH-UZV8-XV99-BPPP6",
      "account": "10544815516691670767",
      "type": "bid"
    },
    {
      "height": 342360,
      "priceNQT": "17599",
      "asset": "17554243582654188572",
      "order": "14259525772251108306",
      "quantityQNT": "35000000",
      "accountRS": "NXT-2EMB-WXXE-EB3N-5NFXH",
      "account": "4032327947670008425",
      "type": "bid"
    },
    {
      "height": 342306,
      "priceNQT": "17584",
      "asset": "17554243582654188572",
      "order": "424004298125144151",
      "quantityQNT": "35890306",
      "accountRS": "NXT-E96C-P3H6-ZJWA-4GZQ8",
      "account": "2547892421111717002",
      "type": "bid"
    },
    {
      "height": 367625,
      "priceNQT": "17501",
      "asset": "17554243582654188572",
      "order": "15590107495417883170",
      "quantityQNT": "25000000",
      "accountRS": "NXT-VK3H-SYBR-W5CW-AE8D9",
      "account": "9488183672507843631",
      "type": "bid"
    },
    {
      "height": 366575,
      "priceNQT": "17500",
      "asset": "17554243582654188572",
      "order": "4079712717057440119",
      "quantityQNT": "1800000000",
      "accountRS": "NXT-USU4-92UY-KEYT-4H649",
      "account": "2560453859745817410",
      "type": "bid"
    },
    {
      "height": 367779,
      "priceNQT": "17400",
      "asset": "17554243582654188572",
      "order": "16844942590458678004",
      "quantityQNT": "50000000",
      "accountRS": "NXT-YBRH-UZV8-XV99-BPPP6",
      "account": "10544815516691670767",
      "type": "bid"
    },
    {
      "height": 362680,
      "priceNQT": "17276",
      "asset": "17554243582654188572",
      "order": "4386747232506842254",
      "quantityQNT": "1150000000",
      "accountRS": "NXT-GX4W-RXGW-7PG5-3AJ84",
      "account": "1232307066044380252",
      "type": "bid"
    },
    {
      "height": 367779,
      "priceNQT": "17200",
      "asset": "17554243582654188572",
      "order": "16289368427593537868",
      "quantityQNT": "50000000",
      "accountRS": "NXT-YBRH-UZV8-XV99-BPPP6",
      "account": "10544815516691670767",
      "type": "bid"
    },
    {
      "height": 342132,
      "priceNQT": "17088",
      "asset": "17554243582654188572",
      "order": "3899937958823857299",
      "quantityQNT": "46709540",
      "accountRS": "NXT-E96C-P3H6-ZJWA-4GZQ8",
      "account": "2547892421111717002",
      "type": "bid"
    },
    {
      "height": 365443,
      "priceNQT": "17001",
      "asset": "17554243582654188572",
      "order": "11975118697702024389",
      "quantityQNT": "10000000",
      "accountRS": "NXT-EGGB-SLRL-B9KT-2CGSW",
      "account": "1036332335372843465",
      "type": "bid"
    },
    {
      "height": 342956,
      "priceNQT": "17000",
      "asset": "17554243582654188572",
      "order": "1969933842674559904",
      "quantityQNT": "100000000",
      "accountRS": "NXT-94AS-QXH6-QJ4R-4FMXP",
      "account": "3095782428191721752",
      "type": "bid"
    },
    {
      "height": 367779,
      "priceNQT": "17000",
      "asset": "17554243582654188572",
      "order": "3854498098342149701",
      "quantityQNT": "50000000",
      "accountRS": "NXT-YBRH-UZV8-XV99-BPPP6",
      "account": "10544815516691670767",
      "type": "bid"
    },
    {
      "height": 366990,
      "priceNQT": "16983",
      "asset": "17554243582654188572",
      "order": "1751068379329626776",
      "quantityQNT": "100000000",
      "accountRS": "NXT-M2Y7-FKFH-CAQC-DEF7N",
      "account": "13408813046948266949",
      "type": "bid"
    },
    {
      "height": 367779,
      "priceNQT": "16800",
      "asset": "17554243582654188572",
      "order": "8894536742612555242",
      "quantityQNT": "50000000",
      "accountRS": "NXT-YBRH-UZV8-XV99-BPPP6",
      "account": "10544815516691670767",
      "type": "bid"
    },
    {
      "height": 367779,
      "priceNQT": "16600",
      "asset": "17554243582654188572",
      "order": "6939763191045086946",
      "quantityQNT": "50000000",
      "accountRS": "NXT-YBRH-UZV8-XV99-BPPP6",
      "account": "10544815516691670767",
      "type": "bid"
    },
    {
      "height": 362680,
      "priceNQT": "16454",
      "asset": "17554243582654188572",
      "order": "2300377208103440678",
      "quantityQNT": "1200000000",
      "accountRS": "NXT-GX4W-RXGW-7PG5-3AJ84",
      "account": "1232307066044380252",
      "type": "bid"
    },
    {
      "height": 367779,
      "priceNQT": "16400",
      "asset": "17554243582654188572",
      "order": "13755531385651713401",
      "quantityQNT": "50000000",
      "accountRS": "NXT-YBRH-UZV8-XV99-BPPP6",
      "account": "10544815516691670767",
      "type": "bid"
    },
    {
      "height": 341669,
      "priceNQT": "16302",
      "asset": "17554243582654188572",
      "order": "7615000821518117440",
      "quantityQNT": "100000000",
      "accountRS": "NXT-ECBU-G3RB-W4HN-DD75D",
      "account": "13082019516984469818",
      "type": "bid"
    },
    {
      "height": 361605,
      "priceNQT": "16300",
      "asset": "17554243582654188572",
      "order": "18200508578570259990",
      "quantityQNT": "1430571",
      "accountRS": "NXT-YL78-U9PB-AUHL-HUJU8",
      "account": "17539861187634612390",
      "type": "bid"
    },
    {
      "height": 367779,
      "priceNQT": "16200",
      "asset": "17554243582654188572",
      "order": "7111664772472907994",
      "quantityQNT": "50000000",
      "accountRS": "NXT-YBRH-UZV8-XV99-BPPP6",
      "account": "10544815516691670767",
      "type": "bid"
    },
    {
      "height": 366992,
      "priceNQT": "16030",
      "asset": "17554243582654188572",
      "order": "3864911588449363971",
      "quantityQNT": "83167137",
      "accountRS": "NXT-J2MS-AZL3-85VQ-DBRDQ",
      "account": "13487974428999942776",
      "type": "bid"
    },
    {
      "height": 365488,
      "priceNQT": "16001",
      "asset": "17554243582654188572",
      "order": "3183019915432676299",
      "quantityQNT": "10000000",
      "accountRS": "NXT-EGGB-SLRL-B9KT-2CGSW",
      "account": "1036332335372843465",
      "type": "bid"
    },
    {
      "height": 342956,
      "priceNQT": "16000",
      "asset": "17554243582654188572",
      "order": "11671105717087937546",
      "quantityQNT": "100000000",
      "accountRS": "NXT-94AS-QXH6-QJ4R-4FMXP",
      "account": "3095782428191721752",
      "type": "bid"
    },
    {
      "height": 367779,
      "priceNQT": "16000",
      "asset": "17554243582654188572",
      "order": "14686413850292294858",
      "quantityQNT": "50000000",
      "accountRS": "NXT-YBRH-UZV8-XV99-BPPP6",
      "account": "10544815516691670767",
      "type": "bid"
    },
    {
      "height": 362680,
      "priceNQT": "15670",
      "asset": "17554243582654188572",
      "order": "17072238928081784259",
      "quantityQNT": "1250000000",
      "accountRS": "NXT-GX4W-RXGW-7PG5-3AJ84",
      "account": "1232307066044380252",
      "type": "bid"
    },
    {
      "height": 330579,
      "priceNQT": "15500",
      "asset": "17554243582654188572",
      "order": "12842438591605720826",
      "quantityQNT": "30802025",
      "accountRS": "NXT-ECBU-G3RB-W4HN-DD75D",
      "account": "13082019516984469818",
      "type": "bid"
    },
    {
      "height": 334488,
      "priceNQT": "15100",
      "asset": "17554243582654188572",
      "order": "1308503478094145915",
      "quantityQNT": "200000000",
      "accountRS": "NXT-ECBU-G3RB-W4HN-DD75D",
      "account": "13082019516984469818",
      "type": "bid"
    },
    {
      "height": 340995,
      "priceNQT": "15000",
      "asset": "17554243582654188572",
      "order": "15255646168110281076",
      "quantityQNT": "100000000",
      "accountRS": "NXT-94AS-QXH6-QJ4R-4FMXP",
      "account": "3095782428191721752",
      "type": "bid"
    },
    {
      "height": 362680,
      "priceNQT": "14924",
      "asset": "17554243582654188572",
      "order": "2184886052265546411",
      "quantityQNT": "1350000000",
      "accountRS": "NXT-GX4W-RXGW-7PG5-3AJ84",
      "account": "1232307066044380252",
      "type": "bid"
    },
    {
      "height": 326997,
      "priceNQT": "14666",
      "asset": "17554243582654188572",
      "order": "3243066919917045823",
      "quantityQNT": "24242400",
      "accountRS": "NXT-TEEN-G2QW-WGQD-72TAX",
      "account": "6819329926945878420",
      "type": "bid"
    },
    {
      "height": 345764,
      "priceNQT": "14311",
      "asset": "17554243582654188572",
      "order": "6151289513850500949",
      "quantityQNT": "5000000",
      "accountRS": "NXT-S8VH-FNMK-P8BF-DKZW5",
      "account": "12822858015163816815",
      "type": "bid"
    },
    {
      "height": 324130,
      "priceNQT": "14310",
      "asset": "17554243582654188572",
      "order": "3007156436307390311",
      "quantityQNT": "20000000",
      "accountRS": "NXT-ECBU-G3RB-W4HN-DD75D",
      "account": "13082019516984469818",
      "type": "bid"
    },
    {
      "height": 324123,
      "priceNQT": "14300",
      "asset": "17554243582654188572",
      "order": "9071260480584212153",
      "quantityQNT": "200000000",
      "accountRS": "NXT-ECBU-G3RB-W4HN-DD75D",
      "account": "13082019516984469818",
      "type": "bid"
    },
    {
      "height": 365463,
      "priceNQT": "14213",
      "asset": "17554243582654188572",
      "order": "11843523648604884625",
      "quantityQNT": "1400000000",
      "accountRS": "NXT-GX4W-RXGW-7PG5-3AJ84",
      "account": "1232307066044380252",
      "type": "bid"
    },
    {
      "height": 365463,
      "priceNQT": "13536",
      "asset": "17554243582654188572",
      "order": "6897841371864263973",
      "quantityQNT": "1450000000",
      "accountRS": "NXT-GX4W-RXGW-7PG5-3AJ84",
      "account": "1232307066044380252",
      "type": "bid"
    },
    {
      "height": 289580,
      "priceNQT": "11",
      "asset": "17554243582654188572",
      "order": "16918880147736176873",
      "quantityQNT": "10000000000",
      "accountRS": "NXT-FAHJ-MNJE-5BHY-24PP4",
      "account": "96443237778498032",
      "type": "bid"
    },
    {
      "height": 283130,
      "priceNQT": "1",
      "asset": "17554243582654188572",
      "order": "3409888667133338290",
      "quantityQNT": "1900100000000",
      "accountRS": "NXT-QELH-XCJW-TLXS-5NKXE",
      "account": "3924382307641406031",
      "type": "bid"
    },
    {
      "height": 328217,
      "priceNQT": "1",
      "asset": "17554243582654188572",
      "order": "4846038567639687043",
      "quantityQNT": "1000000000",
      "accountRS": "NXT-4FLF-V8J8-KXHF-FZGJ5",
      "account": "15114607950223717965",
      "type": "bid"
    }
  ]
};