var Fixtures = Fixtures || {};

Fixtures.getAccount = {
  "publicKey": "0e89a92f502be03444defc640ebf4d44bbf911c42bff618d053493b51b347161",
  "requestProcessingTime": 1,
  "assetBalances": [{
    "asset": "12071612744977229797",
    "balanceQNT": "300"
  }, {
    "asset": "17554243582654188572",
    "balanceQNT": "100000000"
  }, {
    "asset": "12658817572699179955",
    "balanceQNT": "61"
  }, {
    "asset": "16212446818542881180",
    "balanceQNT": "1"
  }],
  "guaranteedBalanceNQT": "995115040",
  "balanceNQT": "995115040",
  "accountRS": "NXT-KAQY-453M-FXE6-2FAF2",
  "unconfirmedAssetBalances": [{
    "asset": "12071612744977229797",
    "unconfirmedBalanceQNT": "300"
  }, {
    "asset": "17554243582654188572",
    "unconfirmedBalanceQNT": "100000000"
  }, {
    "asset": "12658817572699179955",
    "unconfirmedBalanceQNT": "61"
  }, {
    "asset": "16212446818542881180",
    "unconfirmedBalanceQNT": "1"
  }],
  "account": "14932539411571422",
  "effectiveBalanceNXT": 9,
  "unconfirmedBalanceNQT": "400215040",
  "forgedBalanceNQT": "0"
};