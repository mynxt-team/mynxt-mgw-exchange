var Fixtures = Fixtures || {};

Fixtures.getAccountAssets = {
  "requestProcessingTime": 0,
  "accountAssets": [
    {
      "decimals": 8,
      "asset": "17554243582654188572",
      "quantityQNT": "100000000",
      "name": "mgwBTC",
      "unconfirmedQuantityQNT": "100000000"
    }
  ]
};