var Fixtures = Fixtures || {};

Fixtures.getAccounts = {
  "status": "success",
  "data": {
    "accounts": [
      {
        "id_account": "1",
        "tx_account_id": "14305241101586882346",
        "tx_public_key": "",
        "bl_selected": "1",
        "tx_label": "test 1",
        "bl_active": "1",
        "tx_account_rs": "NXT-GRTC-ZP98-VR4A-EEM3F"
      },
      {
        "id_account": "2",
        "tx_account_id": "17226024281148770062",
        "tx_public_key": "",
        "bl_selected": "0",
        "tx_label": "test 2",
        "bl_active": "1",
        "tx_account_rs": "NXT-2KSG-U5X4-P8Y8-G3T5Y"
      }
    ]
  }
};