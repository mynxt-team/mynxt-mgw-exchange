describe('MainController', function () {
  beforeEach(angular.mock.module('mynxt-mgw-exchange'));

  var scope;
  var controller;
  var $httpBackend;
  var MyNXT;

  beforeEach(angular.mock.inject(function ($rootScope, appConfig, $controller, _$httpBackend_, _MyNXT_) {
    scope = $rootScope.$new();
    controller = $controller('SendController', {$scope: scope});
    $httpBackend = _$httpBackend_;
    MyNXT = _MyNXT_;

    $httpBackend.whenPOST(appConfig.apiURL + '/nxt?asset=17554243582654188572&requestType=getAskOrders')
      .respond(Fixtures.getAskOrders);
    $httpBackend.whenPOST(appConfig.apiURL + '/nxt?account=14932539411571422&requestType=getAccount')
      .respond(Fixtures.getAccount);
  }));

  it('should have a SendController', function () {
    expect(controller).not.toBe(null);
  });

  describe('calculations', function () {
    beforeEach(function () {
      MyNXT.getAccountData('14932539411571422');
      $httpBackend.flush();
    });

    it('should correctly find the user current balance', function () {
      // we expect to have 10,000,000 QNT balance
      expect(MyNXT.findAssetBalance(scope.coin.asset)).toBe(Math.pow(10, 8));
    });

    it('should have a calculateNeededQNT function', function () {
      expect(controller.calculateNeededQNT).not.toBe(null);
    });

    it('should correctly calculate how much we need', function () {
      var amount = 1; // 1 BTC
      var neededQNT = controller.calculateNeededQNT(amount);
      expect(neededQNT).toBe(0); // we already have 1 BTC

      amount = 2; // 2 BTC
      neededQNT = controller.calculateNeededQNT(amount);
      expect(neededQNT).toBe(Math.pow(10, 8)); // we have 1 BTC, so 1 left
    });

    it('should correctly calculate order price', function () {
      var amount = 1;
      var orderPrice = controller.calculateOrderPrice(amount);
      expect(orderPrice).toBe(0);

      amount = 2;
      orderPrice = controller.calculateOrderPrice(amount);
      expect(orderPrice).toBe(18946.936); // we need to buy 1 BTC
    });

    it('should correctly calculate fee', function () {
      var orderPrice = 10000;
      var fee = controller.calculateFee(orderPrice);

      expect(fee).toBe(13);

      orderPrice = 1;
      fee = controller.calculateFee(orderPrice);
      expect(fee).toBe(4);
    });
  });
});