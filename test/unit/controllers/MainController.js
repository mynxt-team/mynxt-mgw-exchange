describe('MainController', function () {
  beforeEach(angular.mock.module('mynxt-mgw-exchange'));

  var scope;
  var controller;
  var $httpBackend;
  var MyNXT;

  beforeEach(angular.mock.inject(function ($rootScope, appConfig, $controller, _$httpBackend_, _MyNXT_) {
    scope = $rootScope.$new();
    controller = $controller('MainController', {$scope: scope});
    $httpBackend = _$httpBackend_;
    MyNXT = _MyNXT_;

    $httpBackend.whenGET(/https:\/\/wallet.mynxt.info\/api\/0.1\/user\/account/).respond(Fixtures.getAccounts);
    $httpBackend.whenGET(MyNXT.apiURL + 'mgw/status?coin=mgwBTC').respond(Fixtures.mgwStatus);
    $httpBackend.whenPOST(appConfig.apiURL + '/nxt?account=14305241101586882346&requestType=getAccount').respond(Fixtures.getAccount);
    $httpBackend.whenPOST(appConfig.apiURL + '/nxt?account=14305241101586882346&asset=17554243582654188572&requestType=getAccountCurrentBidOrders').respond(Fixtures.getBidOrders);
    $httpBackend.whenPOST(appConfig.apiURL + '/nxt?account=14305241101586882346&asset=17554243582654188572&requestType=getAccountCurrentAskOrders').respond(Fixtures.getAskOrders);
  }));

  it('should have a MainController', function () {
    expect(controller).not.toBe(null);
  });

  it('should set accounts', function () {
    $httpBackend.flush();
    expect(scope.accounts.length).toBe(2);
  });

  describe('selectedAccount', function () {
    it('should set selectedAccount', function () {
      $httpBackend.flush();
      expect(scope.selectedAccount.tx_account_id).toBe('14305241101586882346');
    });

    it('should set a new selectedAccount', function () {
      var account = Fixtures.getAccounts.data.accounts[1];
      scope.changeSelectedAccount(account);

      expect(scope.selectedAccount.tx_account_id).toBe(account.tx_account_id);
    });

    it('should retrieve account data', function () {
      $httpBackend.flush();
      expect(scope.selectedAccountData.assetBalances.length).toBe(4);
    });

    it('should retrieve asset balance', function () {
      $httpBackend.flush();
      expect(scope.coinBalanceQNT).toBe(100000000);
    });
  });
});