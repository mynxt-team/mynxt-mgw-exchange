var gulp = require('gulp');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var connect = require('gulp-connect');
var sourcemaps = require('gulp-sourcemaps');
var gutil = require('gulp-util');
var ngAnnotate = require('gulp-ng-annotate');

var enviroment = 'development';

var paths = {
  js: [
    './bower_components/iframe-resizer/js/iframeResizer.contentWindow.min.js',
    './bower_components/angular/angular.js',
    './bower_components/angular-bootstrap/ui-bootstrap.min.js',
    './bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',
    './bower_components/angular-loading-bar/build/loading-bar.min.js',
    './bower_components/ladda/dist/spin.min.js',
    './bower_components/ladda/dist/ladda.min.js',
    './bower_components/angular-ladda/dist/angular-ladda.min.js',
    './bower_components/angular-notify/dist/angular-notify.min.js',
    './src/app.js',
    './src/components/controllers/*.js',
    './src/components/helpers/*.js',
    './src/components/filters/*.js',
    './src/components/services/*.js'
  ],
  css: [
    './bower_components/bootstrap/dist/css/bootstrap.min.css',
    './bower_components/ladda/dist/ladda-themeless.min.css',
    './bower_components/angular-loading-bar/build/loading-bar.min.css',
    './bower_components/angular-notify/dist/angular-notify.min.css',
    './src/css/*.css'
  ],
  images: [],
  fonts: [
    './bower_components/bootstrap/dist/fonts/*'
  ],
  html: [
    './src/**/*.html'
  ]
};

gulp.task('js', function () {
  if (enviroment === 'development') {
    paths.js = paths.js.concat('./src/config.js');
  }

  gulp.src(paths.js)
    .pipe(sourcemaps.init())
    .pipe(concat('mynxt-mgw.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./src/dist/js'))
    .pipe(connect.reload());
});

gulp.task('css', function () {
  gulp.src(paths.css)
    .pipe(sourcemaps.init())
    .pipe(concat('mynxt-mgw.css'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./src/dist/css'))
    .pipe(connect.reload());
});

gulp.task('images', function () {
  gulp.src(paths.images)
    .pipe(gulp.dest('./src/dist/images'))
    .pipe(connect.reload());
});

gulp.task('fonts', function () {
  gulp.src(paths.fonts)
    .pipe(gulp.dest('./src/dist/fonts'))
    .pipe(connect.reload());
});

gulp.task('webserver', function () {
  connect.server({
    root: 'src',
    livereload: true,
    port: 8000
  });
});

gulp.task('html', function () {
  gulp.src(paths.html)
    .pipe(connect.reload());
});

gulp.task('build', function () {
  enviroment = 'production';

  console.log('build');

  gulp.src(paths.js)
    .pipe(concat('mynxt-mgw.js'))
    .pipe(ngAnnotate())
    .pipe(uglify({
      mangle: false
    }))
    .pipe(gulp.dest('./src/dist/js'));

  gulp.src(paths.css)
    .pipe(concat('mynxt-mgw.css'))
    .pipe(gulp.dest('./src/dist/css'))
});

gulp.task('default', ['js', 'css', 'fonts', 'images', 'webserver'], function () {
  gutil.env.env = 'development';

  gulp.watch(paths.js, ['js']);
  gulp.watch(paths.css, ['css']);
  gulp.watch(paths.fonts, ['fonts']);
  gulp.watch(paths.images, ['images']);
  gulp.watch(paths.html, ['html']);
});

module.exports.paths = paths;